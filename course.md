# „Was ist ein Lernportfolio?“
## …und wie ist der Einsatz in der Lehre?
### Was ist ein Lernportfolio?
Das Lernportfolio ist ein Instrument in der Lehre, welches den Lernenden unterstützt, „Wissen in Sachzusammenhängen zu kontextualisieren und mithilfe einer systematischen Reflexion der eigenen Lernprozesse kritisch zu hinterfragen“ (Quellmelz & Ruschin, 2013, S. 19). 
In einem Lernportfolio findet sich eine Sammlung von Arbeiten der Lernenden, die die eigenen Leistungen, den eigenen Lernfortschritt und Leistungsstand aufzeigen.
In einem Lehr- und Lernarrangement ist die Leistungsüberprüfung ein wesentlicher Bestandteil, der in der Planung Berücksichtigung finden muss. Dabei gibt es verschiedene Prüfungsformate (mündlich, schriftlich, praktisch bzw. summativ oder formativ). Neben den klassischen Prüfungsformen können kleinere Forschungsprojekte, ein Projektbericht, eine Posterpräsentation oder eben ein Lernportfolio Einsatz finden.
Im weiteren Sinne versteht man also unter einem Portfolio ein Lehr-, Lern- und Entwicklungsinstrument. Im engeren Sinne jedoch wird das Portfolio als Beurteilungsinstrument aufgefasst.

### Formen von Lernportfolios
Es werden verschiedene Formen von Portfolios unterschieden:
Ein **Arbeitsportfolio** dient der Diagnose des Lernprozesses und enthält Dokumente, die das Lernen unterstützt haben. Besonders im Fokus steht dabei die Analyse der eigenen Arbeit.
Bei einem **Entwicklungsportfolio** stehen die eigenen Fortschritte im Vordergrund. Anhand der Reflexion der eigenen Arbeiten hilft es besonders, die eigenen Stärken und Schwächen zu erkennen.
In einem **Beurteilungsportfolio** werden die schriftlich festgehaltenen Leistungen gesammelt und dem Dozierenden zur Beurteilung vorgelegt. Es hat formalen Charakter und bezieht sich meist auf ein bestimmtes Seminar.
Ein **Leseportfolio** dokumentiert die eigenen Leseprozesse und –ergebnisse. Darin enthalten sind oftmals die gelesenen Bücher in einer Liste, Leseerfahrungen, eine Rezension über die gelesenen Bücher, Tagebucheinträge aus der Sicht eines Protagonisten u.v.m.  

In der Praxis überlagern sich die Intentionen der unterschiedlichen Formen oftmals, sodass diese nur selten in Reinform zu finden sind.

### Der Einsatz in der Praxis
Lernportfolios können sich auf einen ganzen Studiengang, ein Modul oder eine Studiensequenz beziehen. Auf der einen Seite wird bei der Führung eines Lernportfolios eine selbstbestimmte Auseinandersetzung mit dem Lerngegenstand erfordert. Auf der anderen Seite wird die Beobachtung und Reflexion des eigenen Lernverhaltens und –fortschritts verlangt. Sie begleiten also den Lernprozess, können aber auch als Beurteilung eingesetzt werden.
Um Lernportfolios sinnvoll als Prüfungsform einzusetzen und in den Lernprozess zu integrieren, bedarf es einer Steuerung durch den Lehrenden. Dabei ist es in erster Linie hilfreich für die Lernenden, wenn die Lehrenden einen klaren Aufbau für das Portfolio vorgeben. Die Arbeitsaufträge sollten so klar wie möglich formuliert werden, damit dies zu einer bestmöglichen Selbststeuerung der Lernenden führt.
Um den eigenen Lernfortschritt realistisch einschätzen und am Ende der Lehrveranstaltung zu reflektieren, bietet es sich an, zu Beginn der Veranstaltung einen Einschätzungsbogen zum Wissensstand der vorhandenen Kompetenzen ausfüllen zu lassen. Dieser wird am Ende wiederholt ausgefüllt, um einen möglichen Kompetenzzuwachs (Progression) feststellen zu können. 
Weiterhin sollten die Arbeitsaufträge für das Lernportfolio das erworbene Wissen kritisch hinterfragen.

### Erstellung von Lernportfolios
Für die Erstellung können vier Phasen unterschieden werden: In der *Einführungsphase* wird über die Ziele und Form des Portfolios entschieden. Beim *Sammeln und Zusammentragen von Material*, also in der zweiten Phase, wird Material gesammelt, dass den Lernprozess begleitet hat und Lernergebnisse dokumentiert. Erst in der folgenden *Auswahl- und Überarbeitungsphase* wird das gesammelte Material erneut gesichtet und erneut im Hinblick auf die Relevanz geprüft. So sollten nur aussagekräftige Materialien in das Portfolio aufgenommen werden. Erst in der *Reflexionsphase* sollte der Lernenden über den eigenen Lernprozess, die relevanten Dokumente und Materialien nachdenken und reflektieren. Über die gesamten Phasen hinweg sollte die Zielsetzung im Blick behalten werden.

### Checkliste zum Einsatz eines Lernportfolios
Bei der Planung des Einsatzes eines Lernportfolios müssen die Lehrenden einige wichtige Punkte berücksichtigen:
1.	Was ist das Ziel des Portfolio-Einsatzes? Welche Kompetenzen sollen gefördert werden?
2.	Welche Inhalte soll das Portfolio enthalten?
3.	Wie kann das Portfolio mit der Lehrveranstaltung verknüpft werden?
4.	In welcher Form soll das Portfolio erstellt werden? (Papier, im Ordner, gebunden, online, etc.)
5.	Wann und wie wird das Portfolio eingeführt? Wie hoch ist der Zeitaufwand für die Lernenden? Wie und in welcher Form können die Lernenden begleitet werden?
6.	Welches sind die Beurteilungskriterien? Wie werden diese den Lernenden zugänglich gemacht?

### Bewertung von Lernportfolios
Das Lernportfolio ist eine Prüfungsform, die über das erworbene Wissen hinaus, auch methodische und reflexive Kompetenzen erfasst. Daher ist es besonders bei dieser Prüfungsform sinnvoll, ein Beurteilungsraster einzusetzen. Die Bewertung sollte also kriteriengeleitet stattfinden, indem die relevan-ten Kriterien genau definiert und in Niveaustufen differenziert werden. Quellmalz und Ruschin (2013) haben ein mögliches Beurteilungsraster für ein Port-folio entworfen, welches die Kriterien Problembe-wältigung, Reflexion und Formalia beinhaltet. Dabei nimmt die Reflexion 50 % der Gesamtbewertung ein.
Onlinetools mit der Portfolio-Funktion sind z.B.:
wikiversity.org oder wikispaces.com.

#### Quellen
- Brunner, I. (Ed.). (2006). Das Handbuch Portfolioarbeit: Kon-zepte-Anregungen-Erfahrungen aus Schule und Lehrerbildung. Kallmeyer.
- Quellmelz, M., & Ruschin, S. (2013). Kompetenzorientiert prüfen mit Lernportfolios. journal hochschuldidaktik, 1-2.
Unididaktik, D. (2006). Lernportfolio. Ausgabe, 1, 2006.






